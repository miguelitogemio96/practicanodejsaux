var net = require('net');
const rl = require('./readl');

var client = net.connect({ port: 7000 }, function () {});

client.write('Iniciar Sesión');
rl.on('line', function (entrada) {
    client.write(entrada);
});

client.on('data', function (data) {
    console.log(data.toString());
    data = data.toString().split(' ');
    if(data[0] == "Bienvenido" && data[1] != "al"){
        rl.close();
        client.end();
    }
});

client.on('end', function () {});