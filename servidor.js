const net = require('net');

const bd = [{nombre:'Dan Israel',apellido: 'Copa Lupe',usuario: 'dcopalupe', password: '123456'},
            {nombre:'Jorge Andres',apellido: 'Alanoca Quino',usuario: 'jalanocaquino', password: '1q2w3e4'},
            {nombre:'Ana',apellido: 'Condori Quispe',usuario: 'acondoriquispe', password: '54321'}];

const server = net.createServer(function (connection) {
    connection.on('data', function(data){
        if(data == 'Iniciar Sesión'){
            connection.write('Bienvenido al sistema Lab273');
            connection.write('Ingrese su usuario y contraseña: (username/password)');
        }else{
            console.log('Cliente conectado ' + data);
            data = data.toString().split('/');
            let finduser = false;
            bd.forEach(element => {
                if(element.usuario == data[0]){
                    finduser = true;
                    if(element.password == data[1]){
                        connection.write('Bienvenido ' + element.nombre + " " + element.apellido);
                    }else{
                        connection.write('La contraseña para '+ data[0] + " es incorrecta");
                        connection.write('Ingrese su usuario y contraseña: (username/password)');
                    }
                }
            });
            if(!finduser){  
                connection.write('El usuario '+ data[0] + " es incorrecto o no existe");
                connection.write('Ingrese su usuario y contraseña: (username/password)');
            }
       }
    });
});

server.listen(7000, function () {
    console.log('Servidor corriendo en el puerto 7000');
});